<?php

namespace App\LinkGenerator;

use App\Tool\Text;
use Pimcore\Http\Request\Resolver\DocumentResolver;
use Pimcore\Localization\LocaleServiceInterface;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\ClassDefinition\LinkGeneratorInterface;
use Pimcore\Model\Document;
use Pimcore\Twig\Extension\Templating\PimcoreUrl;
use Symfony\Component\HttpFoundation\RequestStack;
use Pimcore\Model\Document\Service;
use Pimcore\Db;

class AbstractLinkGenerator implements LinkGeneratorInterface
{
    /*
    * @var DocumentResolver
    */
    protected $documentResolver;

    /*
     * @var RequestStack
     */
    protected $requestStack;

    /*
     * @var PimcoreUrl
     */
    protected $pimcoreUrl;

    /*
     * @var LocaleServiceInterface
     */
    protected $localeService;

    /*
     * @var Service|Service\Dao
     */
    private $documentService;

    /*
     * @param DocumentResolver $documentResolver
     * @param RequestStack $requestStack
     * @param PimcoreUrl $pimcoreUrl
     * @param LocaleServiceInterface $localeService
     * @param Service $documentService
     */
    public function __construct(
        DocumentResolver $documentResolver,
        RequestStack $requestStack,
        PimcoreUrl $pimcoreUrl,
        LocaleServiceInterface $localeService,
        Service $documentService
    )
    {
        $this->documentResolver = $documentResolver;
        $this->requestStack = $requestStack;
        $this->pimcoreUrl = $pimcoreUrl;
        $this->localeService = $localeService;
        $this->documentService = $documentService;
    }

    public function generate(object $object, array $params = []): string
    {
        
        $document = new Document();
        if($this->requestStack->getCurrentRequest()) {
            $localePath = $this->requestStack->getCurrentRequest()->getLocale();
        } else {
            $localePath = $params['localeRoot'];
        }

        if ($localePath == 'en') {
            $document = Document::getByPath("/" . $localePath);
        }

        if (!empty($object->getSlug($localePath))) {
            return current($object->getSlug($localePath))->getSlug();
        }

        if ($object instanceof DataObject\News) {
            $routeName = 'news-detail';
            $name = $object->getTitle($localePath) ? $object->getTitle($localePath) : $routeName;
            $documentRoot = $document->getProperty('news_document');
           
            return $this->getLink($object, $params, $name, $routeName, $documentRoot);
        }
        if ($object instanceof DataObject\Category) {
            $routeName = 'category';
            $name = $object->getTitle($localePath) ? $object->getTitle($localePath) : $routeName;
            
            $documentRoot = $document->getProperty('category_document');
           
            return $this->getLink($object, $params, $name, $routeName, $documentRoot);
        }

        throw new \InvalidArgumentException('Given object not found');
    }

    public function getLink($object, $params, $name, $routeName, $documentRoot)
    {
        return DataObject\Service::useInheritedValues(true, function () use ($object, $params, $name, $routeName, $documentRoot) {
            $fullPath = '';

            if (isset($documentRoot) && $documentRoot instanceof Document) {
                $document = $documentRoot;
            } else {
                $document = $this->documentResolver->getDocument($this->requestStack->getCurrentRequest());
            }

            $localeUrlPart = '/' . $document->getFullPath() . '/';
            if ($document && $localeUrlPart !== $document->getFullPath()) {
                // if (isset($params['localeRoot'])) {
                    $fullPath = $document->getFullPath();
                // } else {
                //     $fullPath = substr($document->getFullPath(), strlen($localeUrlPart));
                // }
            }
            $fullPath = trim($fullPath, '/');
            return $this->pimcoreUrl->__invoke(
                [
                'name' => strtolower(Text::toUrl($name)),
                'id' => $object->getId(),
                'path' => 'news' ,
                '_locale' => $params['locale'] ?? null,
            ],
                $routeName,
                true
            );
        });
    }
}