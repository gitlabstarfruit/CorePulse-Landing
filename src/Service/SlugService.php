<?php

namespace App\Service;

use App\Helper\LogHelper;
use Pimcore\Db;

class SlugService
{
    //lấy slug của object id 
    static public function getSlug($objectId)
    {
        $tableName = "object_url_slugs";
        $query = "SELECT slug FROM " . Db::get()->quoteIdentifier($tableName) . " WHERE objectId = :objectId";
        
        $params = [
            'objectId' => $objectId
        ];
    
        $data = Db::get()->fetchAllAssociative($query, $params);
    
        if ($data) {
            $slug = $data[0]['slug'];
            return $slug;
        }
    
        return '';
    }

    static public function getIdBySlug($slug, $position = 'vi')
    {
        $tableName = "object_url_slugs";
        $query = "SELECT objectId FROM  "
            . Db::get()->quoteIdentifier($tableName)
            . " WHERE slug = '"
            . $slug
            . "' AND position = '"
            . $position . "'";

        $params = [];

        $data = Db::get()->fetchAllAssociative($query, $params);

        if ($data) {
            $id = $data[0]['objectId'];
            return $id; 
        }

        return ''; 
    }

}