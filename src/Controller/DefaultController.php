<?php

namespace App\Controller;

use Pimcore\Bundle\AdminBundle\Controller\Admin\LoginController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\Category;
use Pimcore\Model\DataObject\News;
use App\Service\SlugService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bridge\Twig\Attribute\Template;

class DefaultController extends FrontendController
{
    /**
     * @param Request $request
     * @return Response
     */
    #[Template('default/default.html.twig')]
    public function defaultAction(Request $request)
    {
        return [];
    }

    /**
     * Forwards the request to admin login
     */
    public function loginAction(): Response
    {
        return $this->forward(LoginController::class.'::loginCheckAction');
    }
}
