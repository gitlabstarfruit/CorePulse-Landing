<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\Roadmap;
use Symfony\Bridge\Twig\Attribute\Template;

class RoadmapController extends FrontendController
{
    /**
     * @Route("/roadmap", name="roadmap",))
     */
    #[Template('roadmap/roadmap.html.twig')]
    public function roadmapAction()
    {
        $title = 'roadmap';
        $roadmap = new Roadmap\Listing();
        $roadmap->setOrderKey(['date']);
        $roadmap->setOrder(['asc']);
        $result = [];
        
        foreach ($roadmap as $item) {
            $month = date('m', strtotime($item->getDate()));
            $year = date('Y', strtotime($item->getDate()));
            $key = $month . '-' . $year; 
        
            if (!isset($result[$key])) {
                $result[$key] = [];
            }
        
            $result[$key][] = [
                'title' => $item->getTitle(),
                'icon' => $item->getIcon(),
                'type' => $item->getSelectType(),
                'status' => $item->getStatusPerform(), 
                'content' => $item->getContent(),
            ];
        }
          
        return [
            'title' => $title,
            'result' => $result
        ];
    }
}

?>