<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Bundle\AdminBundle\Controller\Admin\LoginController;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bridge\Twig\Attribute\Template;
use Pimcore\Model\DataObject\Category;
use Pimcore\Model\DataObject\News;

class ContentController extends FrontendController
{

    #[Template('content/home.html.twig')]
    public function homeAction()
    {
        // xử lý lấy danh sách chuyên mục
        $category = new Category\Listing;
        $category->setOrderKey('date');
        $category->setOrder('desc');
        $categoryList = $category;

        return [
            'categoryList' => $categoryList
        ];
    }

     /**
     * @Route("/post-active-pane", name="post-active-pane",))
     */
    public function TabsActiveAction(Request $request, \Knp\Component\Pager\PaginatorInterface $paginator)
    {
        if ($request->isMethod("POST")) {
            $categoryId = $request->get('categoryId');
            
            // lấy danh sách bài viết theo danh mục hiện tại
            $news = new News\Listing();
            $conditions = 'categories like ?';
            $params = '%' . $categoryId . '%';;

            $news->setCondition($conditions, $params);
            $news->setOrderKey(['date']);
            $news->setOrder(['desc']);
            $news->setLimit(6);
            $newsList = $news;

            // $newsList = $paginator->paginate(
            //     $newsList,
            //     $request->get('page', 1),
            //     9
            // );
            
            return $this->render('components/post_item.html.twig',[
                'newsList' => $newsList,
                // 'pagination' => $newsList->getPaginationData()
            ]);
        }
    }

    #[Template('services/services.html.twig')]
    public function servicesAction()
    {
        return [];
    }

    #[Template('contact/contact.html.twig')]
    public function contactAction()
    {
        return [];
    }
}