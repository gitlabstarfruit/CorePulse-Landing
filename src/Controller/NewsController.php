<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Pimcore\Model\DataObject\Category;
use Pimcore\Model\DataObject\News;
use Pimcore\Model\DataObject\Data\UrlSlug;
use App\Service\SlugService;
use Symfony\Bridge\Twig\Attribute\Template;

class NewsController extends FrontendController
{
    const NEWS_DEFAULT_DOCUMENT_PROPERTY_NAME = 'news_default_document';
    const ASK_DOCTOR_KEY = 'post';

    public function slugAction(Request $request, $object, UrlSlug $urlSlug)
    {
        $page = $request->get('page') ? $request->get('page') : 1;
        $limit = $request->get('limit') ? $request->get('limit') : 3;

        return $this->forward('App\Controller\NewsController::detailAction', [
            'id' => $object->getId(),
            'limit' => $limit,
            'page' => $page
        ]);
    }
   
    public function slugCategoryAction(Request $request, $object, UrlSlug $urlSlug)
    {
        $page = $request->get('page') ? $request->get('page') : 1;
        $limit = $request->get('limit') ? $request->get('limit') : 3;

        return $this->forward('App\Controller\NewsController::categoryAction', [
            'id' => $object->getId(),
            'limit' => $limit,
            'page' => $page
        ]);
    }

    /**
     * @Route("/news", name="news",))
     */
    #[Template('news/listing.html.twig')]
    public function newsAction(Request $request , \Knp\Component\Pager\PaginatorInterface $paginator)
    {
        $title = 'News';

        // xử lý lấy danh sách chuyên mục
        $category = new Category\Listing();
        $category->setOrderKey(['date']);
        $category->setOrder(['desc']);
        $category->setLimit(10);
        $categoryList = $category;

        // xử lý lấy danh sách 3 bài viết mới nhất
        $news = new News\Listing();
        $news->setOrderKey(['date']);
        $news->setOrder(['desc']);
        $news->setLimit(3);

        $newsLatest1 = $news->getItems(0, 2);
        $newsLatest2 = $news->getItems(3, 1);

        // xử lý lấy 3 bài có lượt xem cao nhất
        $news = new News\Listing();
        $news->setOrderKey(['view']);
        $news->setOrder(['desc']);
        $news->setLimit(3);
        $newsMostRecentList = $news;
    
        // xử lý lấy danh sách bài viết
        $news = new News\Listing();
        $news->setOrderKey(['date']);
        $news->setOrder(['desc']);

        $allNews = $news->getObjects();
        $newsList = array_slice($allNews, 3);

        $newsList = $paginator->paginate(
            $newsList,
            $request->get('page', 1),
            9
        );
        
        return [
            'title' => $title,
            'categoryList' => $categoryList,
            'newsLatest1' => $newsLatest1,
            'newsLatest2' => $newsLatest2,
            'newsMostRecentList' => $newsMostRecentList,

            'newsList' => $newsList,
            'pagination' => $newsList->getPaginationData()
        ];
    }

    /**
     * @Route("/{path}/{name}~n{id}", name="news-detail", requirements={"path"=".*?", "id"="\d+"}), options={"expose"=true}))
     */
    #[Template('news/detail.html.twig')]
    public function detailAction(Request $request , \Knp\Component\Pager\PaginatorInterface $paginator)
    {
        $news = News::getById($request->get('id'));
        $title = $news->getTitle();

        // xử lý tăng lượt view khi có người vào xem
        $viewOld = $news->getView();
        $viewNew = $viewOld +1;
        $news->setView($viewNew);
        $news->save();
        
        // xử lý lấy 5 bài viết liên quan
        $listing = new News\Listing();
        $conditions = '';
        $params = [];

        foreach ($news->getCategories() as $category) {
            $conditions .= ' OR ( id <> ' . $news->getId() . ' AND categories LIKE ? )';
            $params[] = '%' . $category->getId() . '%';
        }

        $conditions = ltrim($conditions, ' OR ');
        $listing->setCondition($conditions, $params);
        $listing->setOrderKey(['date']);
        $listing->setOrder(['desc']);
        $listing->setLimit(5);
        $newsRecentList = $listing->getObjects(); 
     
        // xử lý lấy danh sách bài viết khác
        $listing = new News\Listing();
        $conditions = 'id <> ?';
        $params = [$news->getId()];

        $listing->setCondition($conditions, $params);
        $listing->setOrderKey(['date']);
        $listing->setOrder('desc');
        $newsMore = $listing->getObjects();

        $newsMore = $paginator->paginate(
            $newsMore,
            $request->get('page', 1),
            9
        );
        // dd($newsMore);
        return [
            'title' => $title,
            'news' => $news,
            'newsRecentList' => $newsRecentList,
            'newsMore' => $newsMore,
            'pagination' => $newsMore->getPaginationData()
        ];
    }

    /**
     * @Route("{path}/{name}~ca{id}", name="category-detail", requirements={"path"=".*?", "id"="\d+"})
     */
    #[Template('news/category.html.twig')]
    public function categoryAction(Request $request , \Knp\Component\Pager\PaginatorInterface $paginator)
    {
        $category = Category::getById($request->get('id'));
        $title = $category->getTitle();
        // lấy danh sách bài viết theo chuyên mục hiện tại
        $news = new News\Listing();
        $conditions = 'categories like ?';
        $params = '%' . $category->getId() . '%';;

        $news->setCondition($conditions, $params);
        $news->setOrderKey(['date']);
        $news->setOrder(['desc']);
        $news->setLimit(10);
        $newsList = $news;

        $newsList = $paginator->paginate(
            $newsList,
            $request->get('page', 1),
            9
        );

        return [
            'title' => $title,
            'category' => $category,
            'newsList' => $newsList,
            'pagination' => $newsList->getPaginationData()
       ];
    }

}