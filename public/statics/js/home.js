// Ẩn hiện menu mobile
function showHideMenu() {
  let menuMb = document.getElementById("menu_mb");
  let icon = document.querySelector(".-icon");

  if (menuMb.style.width === "0px" || menuMb.style.width === "") {
    menuMb.style.display = "block";
    menuMb.style.width = "30vw";
    icon.classList.remove("fa-bars");
    icon.classList.add("fa-times");
  } else {
    menuMb.style.width = "0";
    menuMb.style.display = "none";
    icon.classList.remove("fa-times");
    icon.classList.add("fa-bars");
  }
}
// xử lý tabs blog
$(document).ready(function () {
  let firstActiveTabPane = document.querySelector("#tabs_news .tab-pane.active");
  let categoryId = firstActiveTabPane.dataset.id;
  displayPostList(categoryId);

  let buttons = document.querySelectorAll('.active-post');
  buttons.forEach(function (button) {
    button.addEventListener('click', function () {
      let categoryId = button.dataset.id; 
      console.log(categoryId)
      displayPostList(categoryId);
    });
  });
});

function displayPostList(categoryId) {
  $.ajax({
    url: 'post-active-pane',
    type: 'POST',
    data: {
      categoryId: categoryId
    },
    success: function (response) {
      let elementId = `#newsList${categoryId}`;
      $(elementId).html("");
      $(elementId).html(response);
    },
    error: function (error) {
      console.log(error);
    }
  });
}


// xử lý nút back to top
let btnBack = document.getElementById("btnBack");
window.onscroll = function () { scrollFunction() };
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    btnBack.style.display = "block";
  } else {
    btnBack.style.display = "none";
  }
}
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

