<?php

/**
 * Inheritance: no
 * Variants: no
 *
 * Fields Summary:
 * - title [input]
 * - slug [urlSlug]
 * - description [textarea]
 * - content [wysiwyg]
 * - image [image]
 * - date [date]
 * - view [numeric]
 * - categories [manyToManyObjectRelation]
 */

return Pimcore\Model\DataObject\ClassDefinition::__set_state(array(
   'dao' => NULL,
   'id' => 'news',
   'name' => 'News',
   'title' => '',
   'description' => '',
   'creationDate' => NULL,
   'modificationDate' => 1698814626,
   'userOwner' => 2,
   'userModification' => 2,
   'parentClass' => '',
   'implementsInterfaces' => '',
   'listingParentClass' => '',
   'useTraits' => '',
   'listingUseTraits' => '',
   'encryption' => false,
   'encryptedTables' => 
  array (
  ),
   'allowInherit' => false,
   'allowVariants' => false,
   'showVariants' => false,
   'layoutDefinitions' => 
  Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
     'name' => 'pimcore_root',
     'type' => NULL,
     'region' => NULL,
     'title' => NULL,
     'width' => 0,
     'height' => 0,
     'collapsible' => false,
     'collapsed' => false,
     'bodyStyle' => NULL,
     'datatype' => 'layout',
     'children' => 
    array (
      0 => 
      Pimcore\Model\DataObject\ClassDefinition\Layout\Tabpanel::__set_state(array(
         'name' => 'Layout',
         'type' => NULL,
         'region' => NULL,
         'title' => '',
         'width' => '',
         'height' => '',
         'collapsible' => false,
         'collapsed' => false,
         'bodyStyle' => '',
         'datatype' => 'layout',
         'children' => 
        array (
          0 => 
          Pimcore\Model\DataObject\ClassDefinition\Layout\Panel::__set_state(array(
             'name' => 'Layout',
             'type' => NULL,
             'region' => NULL,
             'title' => '',
             'width' => '',
             'height' => '',
             'collapsible' => false,
             'collapsed' => false,
             'bodyStyle' => '',
             'datatype' => 'layout',
             'children' => 
            array (
              0 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
                 'name' => 'title',
                 'title' => 'Title',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'columnLength' => 190,
                 'regex' => '',
                 'regexFlags' => 
                array (
                ),
                 'unique' => false,
                 'showCharCount' => false,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              1 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\UrlSlug::__set_state(array(
                 'name' => 'slug',
                 'title' => 'Slug',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'domainLabelWidth' => NULL,
                 'action' => '\\App\\Controller\\NewsController::slugAction',
                 'availableSites' => 
                array (
                ),
                 'width' => '',
                 'activeDispatchingEvents' => 
                array (
                ),
              )),
              2 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
                 'name' => 'description',
                 'title' => 'Description',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'maxLength' => NULL,
                 'showCharCount' => false,
                 'excludeFromSearchIndex' => false,
                 'height' => '',
                 'width' => '',
              )),
              3 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Wysiwyg::__set_state(array(
                 'name' => 'content',
                 'title' => 'Content',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'toolbarConfig' => '',
                 'excludeFromSearchIndex' => false,
                 'maxCharacters' => '',
                 'height' => '',
                 'width' => '',
              )),
              4 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Image::__set_state(array(
                 'name' => 'image',
                 'title' => 'Image',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'uploadPath' => '',
                 'width' => '',
                 'height' => '',
              )),
              5 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Date::__set_state(array(
                 'name' => 'date',
                 'title' => 'Date',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => NULL,
                 'useCurrentDate' => false,
                 'columnType' => 'bigint(20)',
                 'defaultValueGenerator' => '',
              )),
              6 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\Numeric::__set_state(array(
                 'name' => 'view',
                 'title' => 'View',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => false,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'defaultValue' => 0,
                 'integer' => true,
                 'unsigned' => false,
                 'minValue' => 0.0,
                 'maxValue' => NULL,
                 'unique' => false,
                 'decimalSize' => NULL,
                 'decimalPrecision' => NULL,
                 'width' => '',
                 'defaultValueGenerator' => '',
              )),
              7 => 
              Pimcore\Model\DataObject\ClassDefinition\Data\ManyToManyObjectRelation::__set_state(array(
                 'name' => 'categories',
                 'title' => 'Categories',
                 'tooltip' => '',
                 'mandatory' => false,
                 'noteditable' => false,
                 'index' => false,
                 'locked' => false,
                 'style' => '',
                 'permissions' => NULL,
                 'fieldtype' => '',
                 'relationType' => true,
                 'invisible' => false,
                 'visibleGridView' => false,
                 'visibleSearch' => false,
                 'blockedVarsForExport' => 
                array (
                ),
                 'classes' => 
                array (
                  0 => 
                  array (
                    'classes' => 'Category',
                  ),
                ),
                 'displayMode' => 'grid',
                 'pathFormatterClass' => '',
                 'maxItems' => NULL,
                 'visibleFields' => 
                array (
                ),
                 'allowToCreateNewObject' => false,
                 'allowToClearRelation' => true,
                 'optimizedAdminLoading' => false,
                 'enableTextSelection' => false,
                 'visibleFieldDefinitions' => 
                array (
                ),
                 'width' => '',
                 'height' => '',
              )),
            ),
             'locked' => false,
             'blockedVarsForExport' => 
            array (
            ),
             'fieldtype' => 'panel',
             'layout' => NULL,
             'border' => false,
             'icon' => '',
             'labelWidth' => 100,
             'labelAlign' => 'left',
          )),
        ),
         'locked' => false,
         'blockedVarsForExport' => 
        array (
        ),
         'fieldtype' => 'tabpanel',
         'border' => false,
         'tabPosition' => 'top',
      )),
    ),
     'locked' => false,
     'blockedVarsForExport' => 
    array (
    ),
     'fieldtype' => 'panel',
     'layout' => NULL,
     'border' => false,
     'icon' => NULL,
     'labelWidth' => 100,
     'labelAlign' => 'left',
  )),
   'icon' => '',
   'group' => '',
   'showAppLoggerTab' => false,
   'linkGeneratorReference' => '',
   'previewGeneratorReference' => '',
   'compositeIndices' => 
  array (
  ),
   'showFieldLookup' => false,
   'propertyVisibility' => 
  array (
    'grid' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
    'search' => 
    array (
      'id' => true,
      'key' => false,
      'path' => true,
      'published' => true,
      'modificationDate' => true,
      'creationDate' => true,
    ),
  ),
   'enableGridLocking' => false,
   'deletedDataComponents' => 
  array (
    0 => 
    Pimcore\Model\DataObject\ClassDefinition\Data\Localizedfields::__set_state(array(
       'name' => 'localizedfields',
       'title' => '',
       'tooltip' => NULL,
       'mandatory' => false,
       'noteditable' => false,
       'index' => false,
       'locked' => false,
       'style' => NULL,
       'permissions' => NULL,
       'fieldtype' => '',
       'relationType' => false,
       'invisible' => false,
       'visibleGridView' => true,
       'visibleSearch' => true,
       'blockedVarsForExport' => 
      array (
      ),
       'children' => 
      array (
        0 => 
        Pimcore\Model\DataObject\ClassDefinition\Data\Input::__set_state(array(
           'name' => 'title',
           'title' => 'Title',
           'tooltip' => '',
           'mandatory' => false,
           'noteditable' => false,
           'index' => false,
           'locked' => false,
           'style' => '',
           'permissions' => NULL,
           'fieldtype' => '',
           'relationType' => false,
           'invisible' => false,
           'visibleGridView' => false,
           'visibleSearch' => false,
           'blockedVarsForExport' => 
          array (
          ),
           'defaultValue' => NULL,
           'columnLength' => 190,
           'regex' => '',
           'regexFlags' => 
          array (
          ),
           'unique' => false,
           'showCharCount' => false,
           'width' => '',
           'defaultValueGenerator' => '',
        )),
        1 => 
        Pimcore\Model\DataObject\ClassDefinition\Data\UrlSlug::__set_state(array(
           'name' => 'slug',
           'title' => 'Slug',
           'tooltip' => '',
           'mandatory' => false,
           'noteditable' => false,
           'index' => false,
           'locked' => false,
           'style' => '',
           'permissions' => NULL,
           'fieldtype' => '',
           'relationType' => false,
           'invisible' => false,
           'visibleGridView' => false,
           'visibleSearch' => false,
           'blockedVarsForExport' => 
          array (
          ),
           'domainLabelWidth' => NULL,
           'action' => '\\App\\Controller\\NewsController::slugAction',
           'availableSites' => 
          array (
          ),
           'width' => '',
           'activeDispatchingEvents' => 
          array (
          ),
        )),
        2 => 
        Pimcore\Model\DataObject\ClassDefinition\Data\Textarea::__set_state(array(
           'name' => 'description',
           'title' => 'Description',
           'tooltip' => '',
           'mandatory' => false,
           'noteditable' => false,
           'index' => false,
           'locked' => false,
           'style' => '',
           'permissions' => NULL,
           'fieldtype' => '',
           'relationType' => false,
           'invisible' => false,
           'visibleGridView' => false,
           'visibleSearch' => false,
           'blockedVarsForExport' => 
          array (
          ),
           'maxLength' => NULL,
           'showCharCount' => false,
           'excludeFromSearchIndex' => false,
           'height' => '',
           'width' => '',
        )),
        3 => 
        Pimcore\Model\DataObject\ClassDefinition\Data\Wysiwyg::__set_state(array(
           'name' => 'content',
           'title' => 'Content',
           'tooltip' => '',
           'mandatory' => false,
           'noteditable' => false,
           'index' => false,
           'locked' => false,
           'style' => '',
           'permissions' => NULL,
           'fieldtype' => '',
           'relationType' => false,
           'invisible' => false,
           'visibleGridView' => false,
           'visibleSearch' => false,
           'blockedVarsForExport' => 
          array (
          ),
           'toolbarConfig' => '',
           'excludeFromSearchIndex' => false,
           'maxCharacters' => '',
           'height' => '',
           'width' => '',
        )),
      ),
       'region' => NULL,
       'layout' => NULL,
       'maxTabs' => NULL,
       'border' => false,
       'provideSplitView' => false,
       'tabPosition' => 'top',
       'hideLabelsWhenTabsReached' => NULL,
       'referencedFields' => 
      array (
      ),
       'permissionView' => NULL,
       'permissionEdit' => NULL,
       'labelWidth' => 100,
       'labelAlign' => 'left',
       'fieldDefinitionsCache' => NULL,
    )),
  ),
   'blockedVarsForExport' => 
  array (
  ),
   'fieldDefinitionsCache' => 
  array (
  ),
   'activeDispatchingEvents' => 
  array (
  ),
));
